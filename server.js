const express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var admin = require('./routes/admin');
var register = require('./routes/register');
var kyc = require('./routes/kyc');

var cors= require('cors');
var app = express();

console.log("inside server.js");

const serverConfig = require('./config');
//Middleware
app.use(cors({origin :'*'}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/kycDocs', express.static('kycDocs'));
app.use('/public/uploads', express.static('uploads'));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/admin', admin);
app.use('/register', register);
app.get('/', (req, res) => res.send('Hello Welcome to CSM CIP World!'))

module.exports = app;