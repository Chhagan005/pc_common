var express = require('express');
var router = express.Router();
var Kyc = require('./../lib/kyc').Kyc; // Users Datatype
var _kyc = new Kyc();

var Server = require('./server').Srv; // Server Datatype
var _server = new Server();



router.post("/requestKyc", function (req, res) {
    var kyc_body = req.body['input'];
    var response = { status: 0, message: "message" }
    _kyc.generateKycRequest(kyc_body, response, function () {
        _server.response(res, response);
    });

});


//Get Kyc Data 

router.post('/getKycRequests', function(req, res)
{
     var response = { status: 0, message: "message" }
    _kyc.getKycRequests(req.body, response, function ()
    {
        _server.response(res, response);
    });

})

router.post("/approveKyc", function (req, res) 
{
    var kyc_body = req.body;
    var response = { status: 0, message: "message" }
    _kyc.approveKycRequest(kyc_body, response, function () 
    {
        _server.response(res, response);
    });

});

router.post("/disApproveKyc", function (req, res) 
{
    var kyc_body = req.body;
    var response = { status: 0, message: "message" }
    _kyc.disApproveKycRequest(kyc_body, response, function () 
    {
        _server.response(res, response);
    });

});

module.exports = router;