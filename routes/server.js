var Srv = function Srv() {};
Srv.prototype.response = function(res, status) {
    var body = status;    
    res.setHeader('Content-Type', 'application/json');
    //res.setHeader('Content-Type', '*');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Allow-Methods', 'POST');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.send(body);
}

exports.Srv= Srv;