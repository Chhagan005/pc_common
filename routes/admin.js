var express = require('express');
var router = express.Router();
var Admins = require('./../lib/admin').Admins; // Users Datatype
var _admin = new Admins();
var Server = require('./server').Srv; // Server Datatype
var _server = new Server()


router.post('/userDetails', function (req, res, next) 
{
        var body = req.body;

        var ip = req.headers['x-forwarded-for'] || 
        req.connection.remoteAddress || 
        req.socket.remoteAddress ||
        (req.connection.socket ? req.connection.socket.remoteAddress : null);

        var response = {status: 0 ,message: "message", data: [] }
        if (body.username) 
        {
                _admin.login(body,response, function () 
                {
                        _server.response(res, response);  
                })
        }
        else 
        {
                response["status"]= 401;
                response["message"] = "Please send username";
                _server.response(res, response);
        }
})

router.post('/updateProfile', function (req, res, next) 
{
        var body = req.body['input'];

        var ip = req.ip;//headers['x-forwarded-for'] ||  req.connection.remoteAddress ||  req.socket.remoteAddress || (req.connection.socket ? req.connection.socket.remoteAddress : null);

        var response = { status: 0, message: "message" }
        _admin.profileChange(body,ip,response, function () 
                {
                        _server.response(res, response);  
                })
})


module.exports = router;