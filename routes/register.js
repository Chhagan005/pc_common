var express = require('express');
var router = express.Router();
var Register = require('./../lib/register').Register; // Users Datatype
var _register = new Register();
var Server = require('./server').Srv; // Server Datatype
var _server = new Server();

var FilterApi = require('./../lib/helper/filterApi').FilterApi;
var filterApi=new FilterApi();


router.post('/resendOTP',function(req,res,next)
{
    var body = req.body;
    
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress ||(req.connection.socket ? req.connection.socket.remoteAddress : null);

    var response = {status: 0 ,message: "message"}

    if(!filterApi.resendOTP(body))
    {
        response["status"]= 401;
        response["message"] = "missing POST parameters";
        _server.response(res, response);
    }
    else
    {
        _register.resendOTP(body,ip,response, function () 
        {
            _server.response(res, response);  
        })
    }
})

router.post('/checkUser',function(req,res,next)
{
    var body = req.body['input'];
    
    var ip = req.ip;//req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress ||(req.connection.socket ? req.connection.socket.remoteAddress : null);

    var response = {status: 0 ,message: "message", fastSecretKey: 0,secretKey:0 }

        _register.checkUser(body,ip,response, function () 
        {
            _server.response(res, response);  
        })
    
})


router.post('/verifyOTP',function(req,res,next)
{
    var body = req.body['input'];
    
    var ip = req.ip;//headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress ||(req.connection.socket ? req.connection.socket.remoteAddress : null);

    var response = {status: 0 ,message: "message" }
    if(!filterApi.verifyOTP(body))
    {
        response["status"]= 401;
        response["message"] = "missing POST parameters";
        _server.response(res, response);
    }
    else
    {
        _register.verifyOTP(body,ip,response, function () 
        {
            _server.response(res, response);  
        })
    }
})

router.post('/userRegisterRequest',function(req,res,next)
{
    var body= req.body['input'];
    
    var ip = req.ip;//req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress ||(req.connection.socket ? req.connection.socket.remoteAddress : null);

    var response = {status: 0 ,message: "message" }

    // if(!filterApi.userRegisterRequest(body))
    // {
    //     response["status"]= 401;
    //     response["message"] = "missing POST parameters";
    //     _server.response(res, response);
    // }
    // else
    // {
            _register.registerClientSelf(body,ip,response, function () 
            {
                _server.response(res, response);  
            })
        
       
   // }
});


router.post('/forgetPassword',function(req,res,next)
{
    var body = req.body['input'];
    
    var ip = req.ip;//headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress ||(req.connection.socket ? req.connection.socket.remoteAddress : null);

    var response = {status: 0 ,message: "message", fastSecretKey: 0,secretKey:0 }

    if(!filterApi.forgetPassword(body))
    {
        response["status"]= 401;
        response["message"] = "missing POST parameters";
        _server.response(res, response);

    }
    else
    {
        _userUtils.forgetPassword(body,ip,response, function () 
        {
            _server.response(res, response);  
        })
    }
})

router.post('/verifyForgetPasswordOTP',function(req,res,next)
{
    var body = req.body['input'];
    
    var ip = req.ip;//['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress ||(req.connection.socket ? req.connection.socket.remoteAddress : null);

    var response = {status: 0 ,message: "message" }
    
    if(!filterApi.verifyforgetPasswordOTP(body))
    {
        response["status"]= 401;
        response["message"] = "missing POST parameters";
        _server.response(res, response);
    }
    else
    {
        _userUtils.verifyforgetPasswordOTP(body,ip,response, function () 
        {
            _server.response(res, response);  
        })
    }
})

router.post('/resendOTPforgetPassword',function(req,res,next)
{
    var body= req.body['input'];
    
    var ip = req.ip;//headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress ||(req.connection.socket ? req.connection.socket.remoteAddress : null);

    var response = {status: 0 ,message: "message"}

    if(!filterApi.resendOTPforgetPassword(body))
    {
        response["status"]= 401;
        response["message"] = "missing POST parameters";
        _server.response(res, response);
    }
    else
    {
        _userUtils.resendOTPforgetPassword(body,ip,response, function () 
        {
            _server.response(res, response);  
        })
    }
})

router.post('/resetPassword',function(req,res,next)
{
    var body= req.body['input'];
    
    var ip = req.ip;//headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress ||(req.connection.socket ? req.connection.socket.remoteAddress : null);

    var response = {status: 0 ,message: "message"}

    if(!filterApi.resetPassword(body))
    {
        response["status"]= 401;
        response["message"] = "missing POST parameters";
        _server.response(res, response);
    }
    else
    {
        _userUtils.resetPassword(body,ip,response, function () 
        {
            _server.response(res, response);  
        })
    }
})

router.post('/changePassword',function(req,res,next)
{
    var body = req.body['input'];
    
    var ip = req.ip;//headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress ||(req.connection.socket ? req.connection.socket.remoteAddress : null);

    var response = {status: 0 ,message: "message" }
    
    if(!filterApi.changePassword(body))
    {
        response["status"]= 401;
        response["message"] = "missing POST parameters";
        _server.response(res, response);
    }
    else
    {
        _userUtils.changePassword(body,response, function () 
        {
            _server.response(res, response);  
        })
    }
})

module.exports = router;