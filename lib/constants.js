module.exports = Object.freeze({   
    USER_STATUS_ACTIVE        : 5,
    USER_STATUS_BLOCKED       : 10,
    USER_STATUS_DELETED       : 15
    
});