// database connection
var MSql = require('./helper/MySqlHelper').MSql; 
var md5 = require('md5');
var crypto = require('crypto');

var CONSTANTS = require('./constants');
var moment=require('moment-timezone');
var _mysql = new MSql(null);

var Admins = function () { };

var CONSTANTS = require('./constants');

function getUserLoginDetailsPromise(userName) 
{
    return new Promise(function (resolve, reject) 
    {
        _mysql.getUserLoginDetailsPromise(userName, function (data) 
        {
            if (data) 
            {
                resolve(data);
            }
            else 
            {
                reject();
            }
        })

    });
}

function generateSessionId()
{
    var sha = crypto.createHash('sha256');
    sha.update(Math.random().toString());
    return sha.digest('hex');
};

Admins.prototype.login = function (input,response,callback)
{
        _mysql.getUserLoginDetailsPromise(input.username, function (data) 
        {
            if (data) 
            {
                response["status"]= 200;
                response["message"] = "Sucessfully get user details !!";
                response["data"]=data;
                callback();
                return;  
            }
            else 
            {
                response["status"]= 404;
                response["message"] = "Please Check username !!";
                callback();
                return;  
            }
        })
 
}

Admins.prototype.profileChange = function (input,ip,response,callback)
{
    _mysql.profileChange(input, function (code) 
    {
        switch(code)
        {
            case 1:
            response["status"]= 200;
            response["message"] = "Updated sucessfully";
            break;

            default:
            response["status"]= 402;
            response["message"] = input.paramChange+ " failed to update.";
        }

        callback();
        return;
    
    });
    
}

exports.Admins = Admins;