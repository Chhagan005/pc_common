var MSql = require('./helper/MySqlHelper').MSql; // MariaDb Object
var Emailer = require('./helper/email').Emailer; // MariaDb Object
var _emailer = new Emailer();
var moment=require('moment-timezone');
var _mysql = new MSql(null);
var FilterApi = require('./helper/filterApi').FilterApi;
var filterApi=new FilterApi();

var Kyc = function () { };


function getVerifySessionPromise(input) 
{
    return new Promise(function (resolve, reject) 
    {
        _mysql.verifySession(input, function (ok,role) 
        {
            if (ok) 
            {
                resolve(role);
            }
            else 
            {
                reject();
            }
        });
    });
}


Kyc.prototype.generateKycRequest = function (input, response, callback)
{
    var utc = Date.now();//MILLIS IN Utc OR new Date().getTime();
    utc = utc / 1000;
    var timeStamp = moment.unix(utc).tz('Asia/Kolkata').format('HH:mm:ss DD-MMM-YYYY');

    input.timeStamp=timeStamp;
    
    _mysql.getUserDetails(input.userId,function(userObject)
    {
        if(userObject)
        {

            _mysql.generateKycRequest(input,function(result) 
            {
                if(result)
                {
                    response["status"]="200";
                    response["message"]="KYC Request Generated Successfully";
                    
                    var message="Name:"+userObject.name+"   User Id: "+input.userId+"     TimeStamp:"+input.timeStamp+"    EmailId:"+userObject.email+"     Phone:"+userObject.phone+"   ";
                    _emailer.sendText("info@pcex.io","New Kyc Request",message);
                    callback();
                }
                else
                {
                    response["status"]="400";
                    response["message"]="Error Generating KYC Request";

                    callback();
                }
            });
        }
        else
        {
            response["status"]="402";
            response["message"]="Error Generating KYC Request";
            callback();
        }
    })
    
}


Kyc.prototype.getKycRequests = function (input, response, callback)
{
    if(!filterApi.getKycRequests(input))
    {
        response["status"]= 401;
        response["message"] = "Missing POST Params";
        callback();
        return;
    } 


    sessionManager.getVerifySessionPromise(input).then(function(role)
    {

        var isGod = role == CONSTANTS.ROLE_BROKER;

        var pageNumber=input.pageNumber;
        var pageSize=25;
        var status=input.status;

        var userArray=[];
        var user;
        var superParentId=input.userId;

        if(global.userAdminDetails)
        for(var userId=0;userId<global.userAdminDetails.length;userId++)
        {
            user=global.userAdminDetails[userId];
            if(user)
            {
                if(commonUtils.checkPower(superParentId,userId))
                {
                    userArray.push(userId);
                }
            }
        }

        _mysql.getKycRequests(status,pageNumber,pageSize,userArray,function(result,hasMore,pageNumber)
        {
            if(result)
            {
                var hasAllPowers,directParent,userId;
                //postprocess for data hiding and security
                for(var i=0;i<result.length;i++)
                {
                    userId=parseInt([result[i].userId]);
                    directParent=global.userAdminDetails[userId].parentId;

                    hasAllPowers= isGod || directParent == superParentId;

                    if(!hasAllPowers)
                    {
                        result[i].email=CONSTANTS.HIDDEN_TEXT;
                        result[i].phone=CONSTANTS.HIDDEN_TEXT;
                        result[i].address=CONSTANTS.HIDDEN_TEXT;
                        result[i].addressProof=CONSTANTS.HIDDEN_TEXT;
                        result[i].city=CONSTANTS.HIDDEN_TEXT;
                        result[i].country=CONSTANTS.HIDDEN_TEXT;
                        result[i].documentName=CONSTANTS.HIDDEN_TEXT;
                        result[i].documentProof=CONSTANTS.HIDDEN_TEXT;
                        result[i].swiftCode=CONSTANTS.HIDDEN_TEXT;
                        result[i].bankProof=CONSTANTS.HIDDEN_TEXT;
                        result[i].bankName=CONSTANTS.HIDDEN_TEXT;
                        result[i].accountNumber=CONSTANTS.HIDDEN_TEXT;
                        result[i].ifscCode=CONSTANTS.HIDDEN_TEXT;
                        result[i].documentProof2=CONSTANTS.HIDDEN_TEXT;
                        result[i].bankProof2=CONSTANTS.HIDDEN_TEXT;
                    }
                }

                response["status"] = 200;
                response["message"] = "KYC request fetched";
                response["data"] = result;
                response["pageNumber"]=pageNumber;
                response["hasMore"]=hasMore;
                callback();
                return;
            }
            else
            {
                response["status"]="400";
                response["message"]="Error Listing KYC Request";
            }
        })
        
    },
    function (code) 
    {
        if(code==-1)
        {
            response["status"]= 401;
            response["message"] = "Missing POST Params";
            callback();
            return;
        }
        else
        {
            response["status"] = 406;
            response["message"] = "InValid Session Id";
            callback();
        }
    });    
}


/**
 * return 
 * 0: database error
 * -1: not found
 * 1: success
 */

Kyc.prototype.approveKycRequest = function (input, response, callback)
{
    if(!filterApi.approveKycRequest(input))
    {
        response["status"]= 401;
        response["message"] = "Missing POST Params";
        callback();
        return;
    }

    getVerifySessionPromise(input).then(function (role) 
    {
       if(role==CONSTANTS.ROLE_BROKER)
       {
            if(!commonUtils.checkPower(input.userId,input.childUserId))
            {
                response["status"] = 400;
                response["message"] = "Permission Denied!!!";
                callback();
                return;
            }
            else
            {

                var timeStamp = commonUtils.getTimeStamp();

                _mysql.approveKycRequest(input,timeStamp,function(code) 
                {
                    switch(code)
                    {
                        case 1:
                        response["status"]="200";
                        response["message"]="KYC Request approved";

                        _mysql.getEmailIdFromUserId(input.childUserId,function(email)
                        {
                            if(email)
                            {
                                response["extra"]="Email Sent to user";

                                var message= `<div style="height:auto;width:650px;box-sizing:border-box;font-family:sans-serif,helvetica,arial;margin:0 auto;display: flex;position: relative;">
                                <div style="float:left;height:auto;width:100%;vertical-align:top;border:3px solid #548f90;background:#242424;padding:20px;box-sizing:border-box;margin: 10px;">
                                    <div style="background:#ccc;border-radius:5px;text-align:center;padding:8px">
                                        <img style="width:150px" src="https://www.pcex.io/assets/lib/images/final-logo.png">
                                    </div>
                                    <h1 style="color:#548f90;text-align:center;margin-top:40px;font-size:26px;text-transform:uppercase;margin-bottom:4px">Congratulations!</h1>
                        
                                    <p style="color: #ffffff;text-align: center;">Your profile is now complete!<br> You may login and trade with a demo account</p>
                                    <p style="color: #ffffff;text-align: center;margin: 0px; ">
                                        <a href="https://pcex.io/market"> 
                                        <button style="cursor: pointer;color: #ffffff;text-align: center;background: #548f90;border: 0px;outline: 0px;padding: 10px;border-radius: 3px;margin: 0px auto 20px;cursor: pointer;width: 150px;">Log In</button>
                                        </a>
                                        </p>
                                </div>
                            </div>`;
                            
                        
                                _emailer.sendHtml(email,"PCEX Profile Updated",message);

                            }
                            else
                            {
                                response["extra"]="Failed to send email to user";
                            }
                        })

                        global.userAdminDetails[input.childUserId].kycStatus=1;
                        socketNotifier.kycStatusChanged(input.childUserId,1);
                        logHelper.kycStatusChanged(timeStamp,input.childUserId,1);
                        
                        break;


                        default:
                        response["status"]="402";
                        response["message"]="Unknown error(402)";
                        callback();
                        break;
                    }
                });
            }
       }
       else
       {
            response["status"] = 400;
            response["message"] = "Permission Denied!!! Not a broker";
            callback();
            return;
       }
    },
    function () {
        console.log("getVerifySessionPromise Failed")
        response["status"] = 406;
        response["message"] = "InValid Session Id";
        callback();
    });
}


Kyc.prototype.disApproveKycRequest = function (input, response, callback)
{
    if(!filterApi.disApproveKycRequest(input))
    {
        response["status"]= 401;
        response["message"] = "Missing POST Params";
        callback();
        return;
    }

    getVerifySessionPromise(input).then(function (role) 
    {
        if(role!=CONSTANTS.ROLE_BROKER)
        {
            response["status"] = 400;
            response["message"] = "Permission Denied!!!";
            callback();
            return;
        }

        if(!commonUtils.checkPower(input.userId,input.childUserId))
        {
            response["status"] = 400;
            response["message"] = "Permission Denied!!!";
            callback();
            return;
        }

        _mysql.disApproveKycRequest(input.kycId,input.childUserId,input.remarks,function(code) 
        {
            if(code==1)
            {
                response["status"]="200";
                response["message"]="KYC Request disapproved";

                _mysql.getEmailIdFromUserId(input.childUserId,function(email)
                {
                    if(email)
                    {
                        response["extra"]="Email Sent to user";

                        var message= `<div style="height:auto;width:650px;box-sizing:border-box;font-family:sans-serif,helvetica,arial;margin:0 auto;display: flex;position: relative;">
                        <div style="float:left;height:auto;width:100%;vertical-align:top;border:3px solid #548f90;background:#242424;padding:20px;box-sizing:border-box;margin: 10px;">
                            <div style="background:#ccc;border-radius:5px;text-align:center;padding:8px">
                                <img style="width:150px" src="https://www.pcex.io/assets/lib/images/final-logo.png">
                            </div>
                            <h1 style="color:#548f90;text-align:center;margin-top:40px;font-size:26px;text-transform:uppercase;margin-bottom:4px">OOPS!</h1>
                
                            <p style="color: #ffffff;text-align: center;">Your KYC request has been disapproved!<br> <span style="
                                background: #dc3545!important;
                                padding: 10px;
                                border: 1px solid #861520!important;
                                margin: 15px;
                                display: inline-block;
                                font-size: 13px;
                                text-transform: uppercase;">`+input.remarks+`</span></p>
                            <p style="color: #ffffff;text-align: center;margin: 0px; ">
                                <a href="https://pcex.io/market"> 
                                <button style="cursor: pointer;color: #ffffff;text-align: center;background: #548f90;border: 0px;outline: 0px;padding: 10px;border-radius: 3px;margin: 0px auto 20px;cursor: pointer;width: 150px;">Log In</button>
                                </a>
                                </p>
                        </div>
                    </div>`;
                    
                    _emailer.sendHtml(email,"PCEX Profile Updated",message);

                    }
                    else
                    {
                        response["extra"]="Failed to send email to user";
                    }
                    callback();
                })
                var timeStamp=commonUtils.getTimeStamp();
                global.userAdminDetails[input.childUserId].kycStatus=0;
                socketNotifier.kycStatusChanged(input.childUserId,0);
                logHelper.kycStatusChanged(timeStamp,input.childUserId,0)
            }
            else
            {
                response["status"]="402";
                response["message"]="Unknown error(402)";
                callback();
            }
        });

    },
    function () 
    {
        console.log("getVerifySessionPromise Failed")
        response["status"] = 406;
        response["message"] = "InValid Session Id";
        callback();
    });    
}

exports.Kyc = Kyc;
