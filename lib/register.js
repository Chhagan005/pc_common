var MSql = require('./helper/MySqlHelper').MSql; 

var Emailer = require('./helper/email').Emailer; 
var _emailer = new Emailer();

var CommonUtils = require('./helper/commonUtils').CommonUtils; 
var commonUtils = new CommonUtils();

var md5 = require('md5');
var _mysql = new MSql(null);

var Register = function () { };

/**
 * this method checks if the user already exist or not
 */
Register.prototype.checkUser =function(input,ip,response,callback)
{
    var timeStamp = commonUtils.getTimeStamp();
    if(!commonUtils.testEmailId(input.email))
    {
        response["status"]= 400;
        response["message"] = "Invalid email";
        callback();
        return;
    }

    _mysql.checkUser(input.email,input.phone,function(x)
    {
        switch(x)
        {
            case 0:
            response["status"]= 400;
            response["message"] = "DataBase Error(1)";
            callback();
            return;

            case 1:
            response["status"]= 400;
            response["message"] = "Email id already registered";
            callback();
            return;

            case 2:
            response["status"]= 400;
            response["message"] = "Contact number already registered";
            callback();
            return;
           
        }

        var otp=commonUtils.generateOTP();
        var secretKey=commonUtils.generateSessionId();

        _mysql.insertOTP(secretKey,otp,ip,timeStamp,input,function(id)
        {
            if(id)
            {
                response["status"]=200;
                response["fastSecretKey"]=id;
                response["secretKey"]=secretKey;
                response["message"]=commonUtils.sendRegistrationOTP(input.phone,input.email,input.countryCode,otp);
                callback();
                return;
            }
            else
            {
                response["status"]=402;
                response["message"] = "Unknwon Error(402)";
                callback();
                return;
            }
        })
    })  
}

Register.prototype.verifyOTP = function (input,ip,response,callback)
{
    var timeStamp = commonUtils.getTimeStamp();

    _mysql.verifyOTP(input.fastSecretKey,input.secretKey,input.otp,timeStamp,function(x)
    {
        if(x==1)
        {
            response["status"]= 200;
            response["message"] = "OTP Verified";
            callback();
        }
        else
        {
            response["status"]= 400;
            response["message"] = "Invalid OTP";
            callback();
        }
    })

}

Register.prototype.resendOTP = function (input,ip,response,callback)
{
    _mysql.resendOTP(input.fastSecretKey,input.secretKey,function(code,otp,email,phone,countryCode)
    {
        switch(code)
        {
            case 1:
            response["status"]= 200;
            response["message"]=commonUtils.sendRegistrationOTP(phone,email,countryCode,otp);
            break;
            case 2:
            response["status"]= 400;
            response["message"] = "OTP resend limit reached";
            break;
            default:
            response["status"]= 402;
            response["message"]="Failed to resend OTP";
        }
        
        callback();

    })

}

Register.prototype.registerClientSelf =function (input,ip,response,callback)
{
    _mysql.getClientRegisterDetails(input.fastSecretKey,input.secretKey,function(userObject)
    {
        if(!userObject)
        {
            response["status"]=400;
            response["message"]="Failed to Register";
            callback();
            return;
        }
        else
        {
            var realPassword=input.password;
            var passwordHash= md5(realPassword);

            var timeStamp = commonUtils.getTimeStamp();

            userObject.passwordHash=passwordHash;
            userObject.timeStamp=timeStamp;
            _mysql.createUser(input.fastSecretKey,userObject,function(userObject2)
            {
                if(userObject2)
                {

                    _emailer.sendLoginCredentials(userObject2.email,userObject2.userName,null);

                    var msg2 = '<div style="width:540px;height:auto;padding:10px;background:#fed136;"><div style="width:500px;height:auto;font-family: Candara ;padding:20px;font-size:13px;background:#1c8685;color:#FFFFFF;"><div style="float:left;font-size:33px;font-weight:bold;color:#FFFFFF">PCEX</div><br><br><br><HR style="border:1px solid #251341;"><BR><div style="font-size:20px;color:#FFFFFF"> New registration request recieved at '+userObject2.timeStamp+' </div><br><small style="font-size:13px;color:#FFFFFF;">Dear Admin, <br><br>There is a new Registration request recieved on  The informations are as follow :<br><br><div style="width:92%;padding:20px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;border:1px solid #ebecee;background-color:#FFFFFF; "><TABLE style="width:100%;"><TR><TD style="color:#222222"> <B>Name :</B> '+userObject2.name+'</TD></TR><TR><TD style="color:#222222"> <B>E-mail :</B> '+userObject2.email+'</TD></TR><TR>	<TD style="color:#222222"> <B>Phone :</B> '+userObject2.phone+'</TD></TR><TR>	<TD style="color:#222222"> <B>Country :</B> '+userObject2.country+'</TD></TR><TR>	<TD style="color:#222222"> <B>City :</B> '+userObject2.city+'</TD></TR><TR>	<TD style="color:#222222"> <B>Pincode :</B> '+userObject2.pincode+'</TD></TR><TR>	<TD style="color:#222222"> <B>Register To :</B> '+"CLIENT"+'</TD></TR></TABLE ></div > <BR></div></div > ';
                    _emailer.sendHtml("info@pcex.io","New CLIENT Registration Request From PCEX",msg2);

                   // _serverToServer.sendRegisterClientRequestToOtherServer(userObject2);
                    
                    response["status"]=200;
                    response["message"]="Registration Request Generated";
                    callback();
                }
                else
                {
                    response["status"]=400;
                    response["message"]="Failed to Register";
                    callback();
                }
            });
        }
    })
}


function getVerifySessionPromise(input) 
{
    return new Promise(function (resolve, reject) 
    {
        _mysql.verifySession(input, function (ok) 
        {
            if (ok) 
            {
                resolve();
            }
            else 
            {
                reject();
            }
        });
    });
}

function generateSessionId()
{
    var sha = crypto.createHash('sha256');
    sha.update(Math.random().toString());
    return sha.digest('hex');
};


Register.prototype.forgetPassword =function(input,ip,response,callback)
{
    var utc=Date.now();//MILLIS IN Utc OR new Date().getTime();
    utc=utc/1000;
    var timeStamp = moment.unix(utc).tz('Asia/Kolkata').format('HH:mm:ss DD-MMM-YYYY');
    var re = /\S+@\S+\.\S+/;

    if(!re.test(input.email))
    {
        response["status"]= 400;
        response["message"] = "Invalid email";
        callback();
        return;
    }
    
    _mysql.forgetPasswordCheckUser(input.email,function(userId)
    {
        if(userId == 0)
        {
            response["status"]= 400;
            response["message"] = "Email id not registered";
            callback();
            return;
        }

        var otp=Math.random() * (999999 - 100000) + 100000;
        otp=parseInt(otp);
        var secretKey=generateSessionId();

        _mysql.insertForgetPasswordRequest(secretKey,userId,ip,input.email,otp,timeStamp,function(fastSecretKey)
        {
            if(fastSecretKey)
            {

            //     var message = `<div style="height:auto;width:650px;box-sizing:border-box;font-family:sans-serif,helvetica,arial;margin:0 auto;display: flex;position: relative;">
            //     <div style="float:left;height:auto;width:100%;vertical-align:top;border:3px solid #548f90;background:#242424;padding:20px;box-sizing:border-box;margin: 10px;">
            //         <div style="background:#ccc;border-radius:5px;text-align:center;padding:8px">
            //             <img style="width:150px" src="https://www.pcex.io/assets/lib/images/final-logo.png">
            //         </div>
            //         <p style="color: #ffffff;text-align: center;">To reset your password, enter this code when prompted</p>
            //         <p style="color: #ffffff;text-align: center;margin: 0px; ">
            //             <button style="color: #ffffff;text-align: center;background: #548f90;border: 0px;outline: 0px;padding: 10px;border-radius: 3px;margin: 8px auto 20px;width: 150px;">`+otp+`</button></p>
            //             <p style="color: #ffffff;text-align: center;margin: 8px;font-size: 14px;">Remember not to share your password with anyone!</p>
            //     </div>
            // </div>`;

            var message = `<div style="height:auto;width:650px;box-sizing:border-box;font-family:sans-serif,helvetica,arial;margin:0 auto;display: flex;position: relative;">
            <div style="float:left;height:auto;width:100%;vertical-align:top;border:3px solid #548f90;background:#242424;padding:20px;box-sizing:border-box;margin: 10px;">
                <div style="background:#ccc;border-radius:5px;text-align:center;padding:8px">
                    <img style="width:150px" src="https://www.pcex.io/assets/lib/images/final-logo.png">
                </div>
                <p style="color: #ffffff;text-align: center;">To reset your password, enter this code when prompted</p>
                <p style="color: #ffffff;text-align: center;margin: 0px; ">
                    <button style="color: #ffffff;text-align: center;background: #548f90;border: 0px;outline: 0px;padding: 10px;border-radius: 3px;margin: 0px auto 20px;width: 150px;">`+otp+`</button></p>
                    <p style="color: #ffffff;text-align: center;margin: 8px;font-size: 14px;">Remember not to share your password with anyone!</p>
            </div>
        </div>`;


                _emailer.sendHtml(input.email,"PCEX Reset Password Request",message);

                response["status"]=200;
                response["fastSecretKey"]=fastSecretKey;
                response["secretKey"]=secretKey;
                response["message"]="OTP has been sent to "+input.email;
                callback();
                return;
            }
            else
            {
                response["status"]= 400;
                response["message"] = "DataBase Error(2)";
            }
        })
    })  
}

Register.prototype.verifyforgetPasswordOTP = function (input,ip,response,callback)
{
    var utc=Date.now();//MILLIS IN Utc OR new Date().getTime();
    utc=utc/1000;
    var timeStamp = moment.unix(utc).tz('Asia/Kolkata').format('HH:mm:ss DD-MMM-YYYY');

    _mysql.verifyforgetPasswordOTP(input.fastSecretKey,input.secretKey,input.otp,timeStamp,function(x)
    {
        if(x==1)
        {
            response["status"]= 200;
            response["message"] = "OTP Verified";
            callback();
        }
        else
        {
            response["status"]= 400;
            response["message"] = "Invalid OTP";
            callback();
        }
    })

}


Register.prototype.resendOTPforgetPassword = function (input,ip,response,callback)
{
    _mysql.resendOTPforgetPassword(input.fastSecretKey,input.secretKey,function(code,otp,email)
    {
        switch(code)
        {
            case 1:
            response["status"]= 200;
            var message = `<div style="height:auto;width:650px;box-sizing:border-box;font-family:sans-serif,helvetica,arial;margin:0 auto;display: flex;position: relative;">
            <div style="float:left;height:auto;width:100%;vertical-align:top;border:3px solid #548f90;background:#242424;padding:20px;box-sizing:border-box;margin: 10px;">
                <div style="background:#ccc;border-radius:5px;text-align:center;padding:8px">
                    <img style="width:150px" src="https://www.pcex.io/assets/lib/images/final-logo.png">
                </div>
                <p style="color: #ffffff;text-align: center;">To reset your password, enter this code when prompted</p>
                <p style="color: #ffffff;text-align: center;margin: 0px; ">
                    <button style="color: #ffffff;text-align: center;background: #548f90;border: 0px;outline: 0px;padding: 10px;border-radius: 3px;margin: 0px auto 20px;width: 150px;">`+otp+`</button></p>
                    <p style="color: #ffffff;text-align: center;margin: 8px;font-size: 14px;">Remember not to share your password with anyone!</p>
            </div>
        </div>`;
            _emailer.sendHtml(email,"PCEX Reset Password Request",message);
            response["message"]="OTP has been sent to "+email;
            break;


            case 2:
            response["status"]= 400;
            response["message"] = "OTP resend limit reached";
            break;

            default:
            response["status"]= 400;
            response["message"]="Failed to resend OTP";
        }

        callback();

    })

}


Register.prototype.resetPassword = function (input,ip,response,callback)
{
    var passwordHash=md5(input.password);
    _mysql.resetPassword(input.fastSecretKey,input.secretKey,passwordHash,function(email,userId)
    {
        if(email)
        {
            var message="Your password has been changed Successfully";
            response["status"]= 200;
            response["message"] = message;
            _emailer.sendText(email,"PCEX Password Reset",message);

            // var obj=new Object();
            // obj.userId=userId;
            // obj.passwordHash=passwordHash;
            // _serverToServer.sendChangePasswordRequestToOtherServer(obj);
            
            callback();
        }
        else
        {
            response["status"]= 400;
            response["message"] = "Failed to reset password";
            callback();
        }
    })
}


Register.prototype.changePassword = function (input,response,callback)
{

    getVerifySessionPromise(input).then(function () 
    {
        var oldPasswordHash=md5(input.currentPassword);
        var newPasswordHash=md5(input.newPassword);
        var userId = input.userId;

        _mysql.changePassword(userId,oldPasswordHash,newPasswordHash,function(code)
        {
            switch(code)
            {
                case 1:
                response["status"]= 200;
                response["message"] = "Password Changed";

                // var obj=new Object();
                // obj.userId=userId;
                // obj.passwordHash=newPasswordHash;
                // _serverToServer.sendChangePasswordRequestToOtherServer(obj);

                break;
                case 0:
                response["status"]= 400;
                response["message"] = "Incorrect password";
                break
                default:
                response["status"]= 402;
                response["message"] = "Unknown error occured!!";
            }

            callback();
            return;
        
        });
    },
    function () 
    {
            console.log("getVerifySessionPromise Failed")
            response["status"] = 406;
            response["message"] = "InValid Session Id";
            callback();
    });

    

}

exports.Register = Register;