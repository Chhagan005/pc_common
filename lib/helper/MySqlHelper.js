
var moment = require('moment-timezone');
let mysql = require('mysql');
var CONSTANTS = require('../constants');

const serverConfig = require('../../config');
const queryString = require('query-string');

const commonDb = "PC_Common";
const pccipDb = "PC_CIP";

var connectionDb2;

var MSql = function ()//(db,x) 
{
        this.connection = mysql.createConnection({
            
            // host:serverConfig.database.host,
            // user:serverConfig.database.user,
            // password:serverConfig.database.password,
            // database:serverConfig.database.database,

            host: '127.0.0.1',
            user: 'pccip',
            password: 'pccip',
            database: commonDb

        });    
};


// MSql.prototype.runQuery = function (query, params, callback) {
// console.log("run query",query);
//     this.connectionDb2.query(query, params, function (err, rows) {
//         if (err) {
//             console.log(err);
//         }
//         callback(err, rows)
//     });
// };

/**
 * userIdName : can be userId OR userName
 */
MSql.prototype.getUserDetails = function (userIdName, callback) 
{
    var query = "SELECT userId,userCode,phone,email,fullName,userName,userStatus,kycStatus,country,pincode FROM users WHERE userName = ? OR userId = ?";

    this.connection.query(query, [userIdName,userIdName], (err, rows) => 
    {
        if (err || !rows || rows.length == 0) 
        {
            console.log("getUserDetails FAILED:",err,rows);
            callback(null);
        }
        else
        {
            var userObject=rows[0];
            callback(userObject);
        }
    });
}

MSql.prototype.getUserLoginDetailsPromise = function (userName, callback) {
    var query = "SELECT userId,phone,email,fullName,userName,userStatus,userPassword,kycStatus,city,country,pincode FROM users WHERE userName = ? LIMIT 1";
    console.log("login query:", query,userName);
    this.connection.query(query, [userName], (err, rows) => {
        if (err || !rows || rows.length == 0) 
        {
            console.log("getUserLoginDetailsPromise FAILED:",err,rows);
            callback(null);
        }
        else 
        {

                  callback(rows[0]);
                  return;
        }
    });
}

/**
 * callback
 * 0 =database error
 * 1=email exist
 * 2=phone exist
 * 3=OK
 */
MSql.prototype.checkUser = function (email, phone, callback) 
{
    var query = "SELECT * FROM users where email = ?";

    this.connection.query(query, [email], (err, rows) => 
    {
        if (err) 
        {
            console.log("checkUser FAILED", rows, err);
            callback(0);
            return;
        }
        if (rows && rows.length > 0)
        {
            callback(1);
            return;
        }

        var query2 = "SELECT * FROM users where phone = ?";

        this.connection.query(query2, [phone], function (err1, rows1) 
        {
            if (err1)
            {
                console.log("checkUser FAILED", rows1, err1);
                callback(0);
                return;
            }
            if (rows1 && rows1.length > 0) 
            {
                callback(2);
                return;
            }

            callback(3);
            return;
        })

    });
}

/**
 * returns null if error and row_id if OK
 */
MSql.prototype.insertOTP = function (secretKey, otp, ip, timeStamp, input,callback) 
{
    var query = "INSERT INTO OTP (secretKey,timeStamp,ip,username,name,email,phone,country,countryCode,city,pincode,myOtp) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

    this.connection.query(query, [secretKey, timeStamp, ip,input.username, input.name, input.email, input.phone, input.country, input.countryCode, input.city, input.pincode, otp], function (err, rows) 
    {
        if (!err && rows && rows.insertId) 
        {
            callback(rows.insertId);
            return;
        }
        else 
        {
            console.log("insertOTP FAILED", rows, err);
            callback(null);
            return;
        }
    });
}

/**
 * returns 
 * 1:verified
 * 0:Cannot verify
 */
MSql.prototype.verifyOTP = function (fastSecretKey, secretKey, otp, timeStamp, callback) 
{
    var query = "SELECT myOtp,secretKey,attempts FROM OTP WHERE fastSecretKey = ?";
    var query2;
    this.connection.query(query, [fastSecretKey], (err, rows) => 
    {
        query3 = "UPDATE OTP SET attempts = attempts + 1 WHERE fastSecretKey = ?";
        this.connection.query(query3, [fastSecretKey], function (err3, rows3){});

        if (rows && !err && rows.length > 0 && rows[0] && rows[0].myOtp == otp) 
        {
            if (rows[0].secretKey == secretKey && rows[0].attempts <= 10) 
            {
                query2 = "UPDATE OTP SET verified = 1,verifiedTimeStamp= ? WHERE fastSecretKey = ?";
                this.connection.query(query2, [timeStamp, fastSecretKey], function (err2, rows2) 
                {
                    callback(1);
                });
            }
            else 
            {
                query2 = "UPDATE OTP SET verified = -1 WHERE fastSecretKey = ?";
                this.connection.query(query2, [fastSecretKey], function (err2, rows2)
                {
                    callback(0);
                });   
            }
        }
        else 
        {
            callback(0);
            return;
        }
    });
}

/**
 * returns code
 * 1:OK, we can resend the OTP
 * 2:Limit exhausted
 * 0:DB error
 */
MSql.prototype.resendOTP = function (fastSecretKey, secretKey, callback) 
{
    var query = "SELECT myOtp,secretKey,email,phone,countryCode,sendCount FROM OTP WHERE fastSecretKey = ?";
    this.connection.query(query, [fastSecretKey], (err, rows) => 
    {
        if (rows && !err && rows.length > 0 && rows[0] && rows[0].secretKey == secretKey) 
        {
            if (rows[0].sendCount >= 3) 
            {
                callback(2);
            }
            else 
            {
                var query2 = "UPDATE OTP SET sendCount = sendCount+1 WHERE fastSecretKey = ?";
                this.connection.query(query2, [fastSecretKey], function (err2, rows2) 
                {
                });

                callback(1, rows[0].myOtp, rows[0].email, rows[0].phone, rows[0].countryCode);
            }
        }
        else 
        {
            callback(0);
            return;
        }
    });
}


MSql.prototype.getClientRegisterDetails =function(fastSecretKey,secretKey,callback)
{
    var query = "SELECT secretKey,verified,username,name,email,phone,country,city,timeStamp,pincode,registered FROM OTP WHERE fastSecretKey = ?";

    this.connection.query(query,[fastSecretKey],function (err,rows) 
    {
        if(err || !rows || rows.length==0)
        {
            callback(null);
        }
        else
        {
            if(secretKey == rows[0].secretKey && rows[0].verified == 1 && rows[0].registered == 0)
            {
                var userObject=new Object();
                userObject.userName=rows[0].username;
                userObject.fullName=rows[0].name;
                userObject.email=rows[0].email;
                userObject.phone=rows[0].phone;
                userObject.country=rows[0].country;
                userObject.pincode=rows[0].pincode;
                userObject.userStatus=5;
                userObject.tncStatus=1;

                callback(userObject);
            }
            else
            {
                callback(null);
            }
        }
    })
}


MSql.prototype.createUser = function (fastSecretKey,userObject, callback) 
{
    
    userObject.kycStatus=serverConfig.port==='3333'?1:0;

    var connection=this.connection;

    connection.beginTransaction(function (err) 
    {
        if (err) 
        {
            console.log("createUser ERROR:", err);
            callback(null);
            return;
        }

        var query = "INSERT INTO users (userName,fullName,userStatus,phone,email,userPassword,kycStatus,country,pincode,createTimeStamp) VALUES (?,?,?,?,?,?,?,?,?,?)";
        connection.query(query,[userObject.userName, userObject.fullName, userObject.userStatus, userObject.phone, userObject.email,userObject.passwordHash, userObject.kycStatus, userObject.country, userObject.pincode,userObject.timeStamp], (err, rows) => 
        {
            if(err || !rows || !rows.insertId)
            {
                connection.rollback(function() 
                {
                    console.log("create 234 User ERROR:",err,rows);
                    callback(null); 
                });
                return;
            }
            else
            {

                var id = rows.insertId;

                var query2="UPDATE OTP SET registered = 1 WHERE fastSecretKey = "+fastSecretKey;
            
                connection.query(query2,function(err2,rows2)
                {
                    if(err2 || !rows2)
                    {
                        connection.rollback(function() 
                        {
                            console.log("create 546 User ERROR:",err2,rows2);
                            callback(null); 
                        });
                        return;
                    }
                    else
                    {
                        connection.commit(function (err3) 
                        {
                            if (err3) 
                            {
                                connection.rollback(function() 
                                {
                                    console.log("create 789 User ERROR:",err3,rows3);
                                    callback(null); 
                                });
                                return;
                            }
                            else
                            {
                
                                //userObject.userName=userName;
                                userObject.userId=id;
                               // userObject.userCode=userCode;
                
                                callback(userObject);
                            }
                        });
                    }
                })
            }
        });
    })
}

MSql.prototype.verifySession = function (input, callback) {
    if (input && input.sessionId && input.fastSessionId && input.userId) 
    {
        var query = "SELECT id,sessionId FROM activeSession WHERE userId = ? ORDER BY id DESC LIMIT 1";

        this.connection.query(query, [input.userId], function (err, rows) 
        {
            //console.log(rows);
            if (err || !rows || rows.length == 0) 
            {
                console.log("Verify Session Failed userId NOT FOUND",input.userId);
                callback(false);
            }
            else 
            {
                var ok=rows[0].id == input.fastSessionId && rows[0].sessionId == input.sessionId;

                if(ok==false)
                {
                    console.log("Verify Session Failed",rows[0].id,input.fastSessionId,rows[0].sessionId,input.sessionId);
                }
                callback(ok, parseInt(rows[0]));
            }
        });
    }
    else 
    {
        console.log("input to verify session:","missing params");
        callback(false);
    }
}

MSql.prototype.forgetPasswordCheckUser = function (email, callback) {
    var query = "SELECT userId FROM users WHERE email = ?";
    this.connection.query(query, [email], (err, rows) => {
        if (err || !rows || rows.length == 0) {
            console.log("forgetPasswordCheckUser FAILED", rows, err);
            callback(0);
            return;
        }
        else {
            callback(rows[0].userId);
            return;
        }
    });
}

MSql.prototype.insertForgetPasswordRequest = function (secretKey, userId, ip, email, otp, timeStamp, callback) {
    var query = "INSERT INTO forgetPasswordRequests (secretKey,userId,ip,email,myOtp,timeStamp) VALUES (?,?,?,?,?,?)";

    this.connection.query(query, [secretKey, userId, ip, email, otp, timeStamp], function (err, rows) {
        if (!err && rows && rows.insertId) {
            callback(rows.insertId);
            return;
        }
        else {
            console.log("insertForgetPasswordRequest FAILED", rows, err);
            callback(null);
        }
    });
}

MSql.prototype.verifyforgetPasswordOTP = function (fastSecretKey, secretKey, otp, timeStamp, callback) {
    var query = "SELECT myOtp,secretKey,attempts FROM forgetPasswordRequests WHERE fastSecretKey = ?";
    this.connection.query(query, [fastSecretKey], (err, rows) => {
        var query3 = "UPDATE forgetPasswordRequests SET attempts = attempts + 1 WHERE fastSecretKey = ?";
        this.connection.query(query3, [fastSecretKey], function (err3, rows3) {
        });

        if (!err && rows && rows.length > 0 && rows[0] && rows[0].myOtp == otp) {
            var query2;
            if (rows[0].secretKey == secretKey || rows[0].attempts <= 10) {
                query2 = "UPDATE forgetPasswordRequests SET verified = 1,verifiedTimeStamp= ? WHERE fastSecretKey = ?";
                this.connection.query(query2, [timeStamp, fastSecretKey], function (err2, rows2) {
                });
                callback(1);
            }
            else {
                query2 = "UPDATE forgetPasswordRequests SET verified = -1 WHERE fastSecretKey = ?";
                this.connection.query(query2, [fastSecretKey], function (err2, rows2) {
                });
                callback(0);
            }
        }
        else {
            callback(0);
            return;
        }
    });
}


MSql.prototype.resendOTPforgetPassword = function (fastSecretKey, secretKey, callback) {
    var query = "SELECT myOtp,secretKey,email,sendCount FROM forgetPasswordRequests WHERE fastSecretKey = ?";
    this.connection.query(query, [fastSecretKey], (err, rows) => {
        if (!err && rows && rows.length > 0 && rows[0] && rows[0].secretKey == secretKey) {
            if (rows[0].sendCount >= 3) {
                callback(2);
            }
            else {
                var query2 = "UPDATE forgetPasswordRequests SET sendCount = sendCount+1 WHERE fastSecretKey = ?";
                this.connection.query(query2, [fastSecretKey], function (err2, rows2) {
                });

                callback(1, rows[0].myOtp, rows[0].email);
            }
        }
        else {
            callback(0);
            return;
        }
    });
}

//tested by vishwas 20 Nov 09:23
MSql.prototype.resetPassword = function (fastSecretKey, secretKey, passwordHash, callback) {
    var query = "SELECT secretKey,verified,email,userId FROM forgetPasswordRequests WHERE fastSecretKey = ?";
    this.connection.query(query, [fastSecretKey], (err, rows) => {
        if (!err && rows && rows.length > 0 && rows[0] && rows[0].secretKey == secretKey && rows[0].verified == 1) {
            var query2 = "UPDATE users SET userPassword = ? WHERE userId = ?";
            this.connection.query(query2, [passwordHash, rows[0].userId], function (err2, rows2) { });


            callback(rows[0].email,rows[0].userId);
        }
        else {
            callback(null);
            return;
        }
    });
}

/**
 * 1 success
 * 0 wrong current password
 * -1 database error
 */
MSql.prototype.changePassword = function (userId, oldPasswordHash, newPasswordHash, callback) {
    var query = "SELECT userPassword FROM users WHERE userId = ?";
    this.connection.query(query, [userId], (err, rows) => {
        if (err || !rows || rows.length == 0) {
            console.log("changePassword FAILED", rows, err);
            callback(-1);
            return;
        }
        else {
            if (rows[0].userPassword == oldPasswordHash) {
                var query2 = "UPDATE users SET userPassword = ? WHERE userId = ?";
                this.connection.query(query2, [newPasswordHash, userId], (err2, rows2) => {
                    if (err2) {
                        console.log("changePassword FAILED", err2, rows2);
                        callback(-1);
                        return;
                    }
                    else {
                        callback(1);
                        return;
                    }
                });
            }
            else {
                callback(0);
                return;
            }
        }
    });
}


MSql.prototype.profileChange = function (inputObj,callback) 
{
    var userId = inputObj.userId;
    var paramChange = inputObj.paramChange;
 
    console.log(paramChange);

     var updateString;
     for(var myKey in paramChange) {

        if(updateString)
        {
            updateString+=' , '+myKey+"=\'"+paramChange[myKey]+"\'";
        }
        else
        {
            updateString=myKey+"=\'"+paramChange[myKey]+"\'";
        }   
    }
    
    var query = "UPDATE users SET "+updateString+" WHERE userId = ?";
    this.connection.query(query, [userId], (err, rows) => {
        if (err) {
            console.log("change "+updateString+" FAILED", err, rows);
            callback(0);
            return;
        }
        else {
            callback(1);
            return;
        }
    });

}

MSql.prototype.generateKycRequest = function (input, callback)
{

    //console.log('requested by', input.requestedBy);
    input.userId = input.userId;
    var addressProof = input.addressProof;
    var documentProof = input.documentProof;
    var bankProof = input.bankProof;
    var documentProof2 = input.documentProof2;
    var bankProof2 = input.bankProof2;

    var nomineeStatus = input.nomineeStatus;
    var nomineeMsg = input.nomineeMsg;
    var nomineeName = input.nomineeName;
    var nomineeAge = input.nomineeAge;
    var nomineeRelation = input.nomineeRelation;

    var query = "INSERT INTO KYC (timeStamp, userId, address, addressProof, documentName, documentProof, swiftCode, bankName, accountNumber, ifscCode,bankProof,bankProof2,documentProof2,nomineeName,nomineeAge,nomineeRelation,nomineeStatus,nomineeMsg) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    this.connection.query(query, [input.timeStamp, input.userId, input.address, addressProof, input.documentName, documentProof, input.swiftCode, input.bankName, input.accountNumber, input.ifscCode, bankProof, bankProof2, documentProof2,nomineeName,nomineeAge, nomineeRelation, nomineeStatus, nomineeMsg], function (err, rows) {
        if (!err && rows) {
            callback(true);
        }
        else {
            console.log('generateKycRequest ERROR', err);
            callback(false);
        }
    });
}

exports.MSql = MSql;