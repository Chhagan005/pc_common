
var CommonUtils=function(){};
var crypto = require('crypto');

var moment=require('moment-timezone');

var Emailer = require('./email').Emailer; // MariaDb Object
var _emailer = new Emailer();

var SMS = require('./sms').SMS;
var sms = new SMS()


CommonUtils.prototype.generateSessionId=function()
{
    var sha = crypto.createHash('sha256');
    sha.update(Math.random().toString());
    return sha.digest('hex');
};

CommonUtils.prototype.generateOTP=function()
{
    var otp=Math.random() * (999999 - 100000) + 100000;
    otp=parseInt(otp);

    return otp;
};

CommonUtils.prototype.sendRegistrationOTP=function(phone,email,countryCode,otp)
{
    var msg;

    if(phone.length === 10 && countryCode == "+91")
    {
        msg="OTP has been sent to mobile "+ "+91"+phone;

        var message="Your OTP FOR PCEX Registration Request is : "+otp;
        sms.sendSms(phone,message,function()
        {
        })
    }
    else
    {
        msg="OTP has been sent to email "+email;

        _emailer.sendRegistrationOTP(email,otp);
    }

    return msg;
}

CommonUtils.prototype.generatePassword=function()
{
    var passwordLength=8;
    var text = "";
    var set1="ABCDEFGHJKLMNPQRTUVWXYZabcdefghijkmnpqrstuvwxyz2346789";
    var set2="@#";
    var i2=Math.random() * (passwordLength - 1) + 1;
    i2=parseInt(i2);
    for(var i=0;i<passwordLength;i++)
    {
        if(i===i2)
        {
            text+=set2.charAt(Math.floor(Math.random() * set2.length));
        }
        else
        {
             text+=set1.charAt(Math.floor(Math.random() * set1.length));
        }
    }

    return text;

}

CommonUtils.prototype.getTimeStamp=function()
{
    var utc=Date.now();//MILLIS IN Utc OR new Date().getTime();
    utc=utc/1000;
    var timeStamp = moment.unix(utc).tz('Asia/Kolkata').format('HH:mm:ss DD-MMM-YYYY');

    return timeStamp;
}



/**
 * returns: true if emailId is valid
 */
CommonUtils.prototype.testEmailId=function(email)
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}


CommonUtils.prototype.getMd5=function(str)
{
    var hash= md5(str);
    return hash;
}


exports.CommonUtils= CommonUtils;