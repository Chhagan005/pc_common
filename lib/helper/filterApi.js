var FilterApi = function () { };

var CONSTANTS = require('../constants');


FilterApi.prototype.checkUser = function (input) {
    if (input && input.name && input.email && input.phone && input.country && input.countryCode && input.city && input.pincode) {
        input.email = input.email.toLowerCase();
        input.email = input.email.replace(/ /g, '');
        input.phone = input.phone.replace(/[^0-9]/g, '');


        if (input.phone.startsWith('+91') && input.countryCode == "+91") {
            input.phone = input.phone.replace('+91', '');
        }

        if (input.phone.startsWith('91') && input.phone.length == 12 && input.countryCode == "+91") {
            input.phone = input.phone.replace('91', '');
        }

        return true;
    }
    else {
        return false;
    }
}

FilterApi.prototype.userRegisterRequest = function (input) {
    if (input && input.fastSecretKey && input.secretKey) {

        if (!input.password || input.password.length < 8 || input.password.length > 64) {
            return false;
        }


        return true;
    }
    else {
        return false;
    }
}

FilterApi.prototype.verifyOTP = function (input) {
    if (input && input.fastSecretKey && input.secretKey && input.otp) {
        input.otp = parseInt(input.otp);
        if (input.otp > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

FilterApi.prototype.resendOTP = function (input) {
    if (input && input.fastSecretKey && input.secretKey) {
        return true;
    }
    else {
        return false;
    }
}

FilterApi.prototype.forgetPassword = function (input) {
    if (input && input.email) {
        input.email = input.email.toLowerCase();
        input.email = input.email.replace(/ /g, '');
        return true;
    }
    else {
        return false;
    }
}

FilterApi.prototype.verifyforgetPasswordOTP = function (input) {
    if (input && input.fastSecretKey && input.secretKey && input.otp) {
        input.otp = parseInt(input.otp);
        if (input.otp > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

FilterApi.prototype.resendOTPforgetPassword = function (input) {
    if (input && input.fastSecretKey && input.secretKey) {
        return true;
    }
    else {
        return false;
    }
}

FilterApi.prototype.resetPassword = function (input) {
    if (input && input.fastSecretKey && input.secretKey && input.password) {
        return true;
    }
    else {
        return false;
    }
}

FilterApi.prototype.changePassword = function (input) {
    if (input && input.userId && input.sessionId && input.fastSessionId && input.currentPassword && input.newPassword) {
        input.userId = parseInt(input.userId);

        if (input.userId) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

FilterApi.prototype.getKycRequests = function(input)
{
    if (input && input.sessionId && input.fastSessionId && input.userId && input.pageNumber && (input.status || input.status == 0) ) 
    {
        input.userId=parseInt(input.userId);
        input.fastSessionId=parseInt(input.fastSessionId);

        input.pageNumber=parseInt(input.pageNumber);
        input.status=parseInt(input.status);

        if(input.pageNumber >0 && input.userId>0 && input.fastSessionId>0  )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

FilterApi.prototype.generateKycRequest = function(input)
{
    if (input && input.userId && input.sessionId && input.fastSessionId && input.kycId && input.childUserId) 
    {
        input.userId=parseInt(input.userId);
        input.kycId=parseInt(input.kycId);
        input.childUserId=parseInt(input.childUserId);
        

        if(input.userId >0 && input.kycId > 0 && input.childUserId>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}


FilterApi.prototype.approveKycRequest = function(input)
{
    if (input && input.userId && input.sessionId && input.fastSessionId && input.kycId && input.childUserId) 
    {
        input.userId=parseInt(input.userId);
        input.kycId=parseInt(input.kycId);
        input.childUserId=parseInt(input.childUserId);
        

        if(input.userId >0 && input.kycId > 0 && input.childUserId>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

FilterApi.prototype.disApproveKycRequest = function(input)
{
    if (input && input.userId && input.sessionId && input.fastSessionId && input.kycId && input.childUserId && input.remarks) 
    {
        input.userId=parseInt(input.userId);
        input.kycId=parseInt(input.kycId);
        input.childUserId=parseInt(input.childUserId);

        if(input.userId >0 && input.kycId > 0 && input.childUserId > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}



exports.FilterApi = FilterApi;