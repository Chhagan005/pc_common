var nodemailer = require('nodemailer');



var Emailer=function()
{
    this.transporter = nodemailer.createTransport({
        host: 'smtp.mail.us-west-2.awsapps.com',
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: 
        {
          user: 'admin@pcex.io', // your domain email address
          pass: 'Arun@9717' // your password
        }
      });
	
};

Emailer.prototype.sendText= function(to,subject,message)
{
    var mailOptions = 
    {
        from: 'admin@pcex.io',
        to: to,
        subject: subject,
        text: message
        //html: '<h1>Welcome</h1><p>That was easy!</p>'
    };

    this.transporter.sendMail(mailOptions, function(error, info)
    {
        if (error) {
          console.log("Email ERROR",error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
}
  
Emailer.prototype.sendHtml= function(to,subject,message)
{
    var mailOptions = 
    {
        from: 'admin@pcex.io',
        to: to,
        subject: subject,
        html: message
    };



    this.transporter.sendMail(mailOptions, function(error, info)
    {
        if (error) {
          console.log("Email ERROR",error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });

}
  
Emailer.prototype.sendRegistrationOTP =function(to,otp)
{
  var message= 
            `<div style="height:auto;width:650px;box-sizing:border-box;font-family:sans-serif,helvetica,arial;margin:0 auto;display: flex;position: relative;">
            <div style="float:left;height:auto;width:100%;vertical-align:top;border:3px solid #548f90;background:#242424;padding:20px;box-sizing:border-box;margin: 10px;">
            <div style="background:#ccc;border-radius:5px;text-align:center;padding:8px">
            <img style="width:150px" src="https://www.pcex.io/assets/lib/images/final-logo.png">
            </div>
            <p style="color: #ffffff;text-align: center;">To verify your Email ID, enter this code when prompted</p>
            <p style="color: #ffffff;text-align: center;margin: 0px; ">
            <button style="color: #ffffff;text-align: center;background: #548f90;border: 0px;outline: 0px;padding: 10px;border-radius: 3px;margin: 0px auto 20px;width: 150px;">`+otp+`</button></p>
            </div>
            </div>`;
            
  this.sendHtml(to,"PCEX Registration OTP",message);
}


Emailer.prototype.sendLoginCredentials =function(email,userName,realPassword)
{
    if(!realPassword)
    {
        realPassword="********";
    }

    var msg= `<div style="height:auto;width:650px;box-sizing:border-box;font-family:sans-serif,helvetica,arial;margin:0 auto;display: flex;position: relative;">
		<div style="float:left;height:auto;width:100%;vertical-align:top;border:3px solid #548f90;background:#242424;padding:20px;box-sizing:border-box;margin: 10px;">
			<div style="background:#ccc;border-radius:5px;text-align:center;padding:8px">
				<img style="width:150px" src="https://www.pcex.io/assets/lib/images/final-logo1.png">
			</div>
			<h1 style="color:#548f90;text-align:center;margin-top:40px;font-size:26px;text-transform:uppercase;margin-bottom:4px">Welcome to your PCEX account!</h1>
			<p style="color: #ffffff;text-align: center;margin: 0px;"> Your Username and Password are:</p>
			<div style="font-size: 14px;background: #ccc;border-radius: 5px;text-align: left;padding:15px;margin: 15px 0px;">
				<p style="margin-bottom: 0px;margin-top: 0px;"><strong>Username:</strong> <span>`+userName+`</span></p>
				<p style="margin-bottom: 0px;"><strong>Password:</strong> <span>`+realPassword+`</span></p>
			</div>
			<p style="color: #ffffff;text-align: center;margin: 0px;font-size: 14px;">Trading on PCEX is now live! You can also test our Demo account that has been credited with $100,000 and 10,000 FCC.</p>
			<p style="color: #ffffff;text-align: center;margin: 0px; ">
            <a href="https://www.pcex.io/market">
				<button style="cursor: pointer;color: #ffffff;text-align: center;background: #548f90;border: 0px;outline: 0px;padding: 10px;border-radius: 3px;margin: 20px auto 20px;cursor: pointer;width: 150px;">Trade Now</button>
                </a>    
            <p style="color: #ffffff;text-align: center;margin: 0px;font-size: 13px;"> Please remember to change your password once you log in.
            <p style="color: #ffffff;text-align: center;margin: 0px;font-size: 13px;"> Let us know how we can help you better! 
            <br> <br>
            <p style="color: #ffffff;text-align: center;margin: 0px;font-size: 13px;"> You can send us a question: 
				<a href="mailto:info@pcex.io" target="_top" style="color: inherit;text-decoration: none;">info@pcex.io</a>
			</p>
		</div>
    </div>`;
    
 
    this.sendHtml(email,"CIP Login Credentials",msg);
}

Emailer.prototype.sendLoginCredentialsUpperMembers =function(email,userName,realPassword,roleName,childNameStr)
{
    if(!realPassword)
    {
        realPassword="********";
    }

    var msg= `<div style="height:auto;width:650px;box-sizing:border-box;font-family:sans-serif,helvetica,arial;margin:0 auto;display: flex;position: relative;">
		<div style="float:left;height:auto;width:100%;vertical-align:top;border:3px solid #548f90;background:#242424;padding:20px;box-sizing:border-box;margin: 10px;">
			<div style="background:#ccc;border-radius:5px;text-align:center;padding:8px">
				<img style="width:150px" src="https://www.pcex.io/assets/lib/images/final-logo1.png">
			</div>
			<h1 style="color:#548f90;text-align:center;margin-top:40px;font-size:26px;text-transform:uppercase;margin-bottom:4px">Welcome to your PCEX account!</h1>
			<p style="color: #ffffff;text-align: center;margin: 0px;"> As a `+roleName+` for PCEX, your Username and Password are:</p>
			<div style="font-size: 14px;background: #ccc;border-radius: 5px;text-align: left;padding:15px;margin: 15px 0px;">
				<p style="margin-bottom: 0px;margin-top: 0px;"><strong>Username:</strong> <span>`+userName+`</span></p>
				<p style="margin-bottom: 0px;"><strong>Password:</strong> <span>`+realPassword+`</span></p>
			</div>
			<p style="color: #ffffff;text-align: center;margin: 0px;font-size: 14px;">Trading on PCEX is now live! You can also test our Demo account that has been credited with $100,000 and 10,000 FCC.</p>
			<p style="color: #ffffff;text-align: center;margin: 0px; ">
            <a href="https://www.pcex.io/market">
				<button style="cursor: pointer;color: #ffffff;text-align: center;background: #548f90;border: 0px;outline: 0px;padding: 10px;border-radius: 3px;margin: 20px auto 20px;cursor: pointer;width: 150px;">Trade Now</button>
                </a>    
            <p style="color: #ffffff;text-align: center;margin: 0px;font-size: 13px;"> Please remember to change your password once you log in. You can now add `+childNameStr+` to your PCEX account.
            <p style="color: #ffffff;text-align: center;margin: 0px;font-size: 13px;"> Let us know how we can help you better! 
            <br> <br>
            <p style="color: #ffffff;text-align: center;margin: 0px;font-size: 13px;"> You can send us a question: 
				<a href="mailto:info@pcex.io" target="_top" style="color: inherit;text-decoration: none;">info@pcex.io</a>
			</p>
		</div>
    </div>`;
    
 
    this.sendHtml(email,"PCEX Login Credentials",msg);
}

Emailer.prototype.heirarchyMemberApproval = function(name, email)
{
    

    var msg= `<div style="height:auto;width:650px;box-sizing:border-box;font-family:sans-serif,helvetica,arial;margin:0 auto;display: flex;position: relative;">
		<div style="float:left;height:auto;width:100%;vertical-align:top;border:3px solid #548f90;background:#242424;padding:20px;box-sizing:border-box;margin: 10px;">
			<div style="background:#ccc;border-radius:5px;text-align:center;padding:8px">
				<img style="width:150px" src="https://www.pcex.io/assets/lib/images/final-logo1.png">
			</div>
			<h1 style="color:#548f90;text-align:center;margin-top:40px;font-size:26px;text-transform:uppercase;margin-bottom:4px">Congratulation !</h1>
      <p style="color: #ffffff;text-align: center;margin: 0px; font-size:20px;">Dear `+name+`</p>
      <p style="color: #ffffff;text-align: center;margin: 0px;">Your Account has been approved:</p>
		
			<p style="color: #ffffff;text-align: center;margin: 0px;font-size: 14px;">Demo trading on PCEX is now live! Your virtual account has been credited with $100,000 and 10,000 FCC.</p>
			<p style="color: #ffffff;text-align: center;margin: 0px; ">
            <a href="https://www.pcex.io/market">
				<button style="cursor: pointer;color: #ffffff;text-align: center;background: #548f90;border: 0px;outline: 0px;padding: 10px;border-radius: 3px;margin: 20px auto 20px;width: 150px;">Trade Now</button>
                </a>    
            <p style="color: #ffffff;text-align: center;margin: 0px;font-size: 13px;">	Let us know how we can help you better! You can send us a question: 
				<a href="mailto:info@pcex.io" target="_top" style="color: inherit;text-decoration: none;">info@pcex.io</a>
			</p>
		</div>
    </div>`;
    
 
    this.sendHtml(email,"PCEX Membership Approval",msg);
}

Emailer.prototype.heirarchyMemberDisapproval = function(name, email, remarks)
{
    

    var msg= `<div style="height:auto;width:650px;box-sizing:border-box;font-family:sans-serif,helvetica,arial;margin:0 auto;display: flex;position: relative;">
		<div style="float:left;height:auto;width:100%;vertical-align:top;border:3px solid #548f90;background:#242424;padding:20px;box-sizing:border-box;margin: 10px;">
			<div style="background:#ccc;border-radius:5px;text-align:center;padding:8px">
				<img style="width:150px" src="https://www.pcex.io/assets/lib/images/final-logo1.png">
			</div>
			<h1 style="color:#548f90;text-align:center;margin-top:40px;font-size:26px;text-transform:uppercase;margin-bottom:4px">Sorry !</h1>
      <p style="color: #ffffff;text-align: center;margin: 0px; font-size:20px;">Dear `+name+`</p>
      <p style="color: #ffffff;text-align: center;margin: 0px;">Your Account has been Suspended due to below reason</p>
      <p style="color: #ffffff;text-align: center;margin: 0px;">`+remarks+`</p>
			<p style="color: #ffffff;text-align: center;margin: 0px;font-size: 14px;">Demo trading on PCEX is now live! Your virtual account has been credited with $100,000 and 10,000 FCC.</p>
			<p style="color: #ffffff;text-align: center;margin: 0px; ">
            <a href="https://www.pcex.io/market">
				<button style="cursor: pointer;color: #ffffff;text-align: center;background: #548f90;border: 0px;outline: 0px;padding: 10px;border-radius: 3px;margin: 20px auto 20px;width: 150px;">Trade Now</button>
                </a>    
            <p style="color: #ffffff;text-align: center;margin: 0px;font-size: 13px;">	Let us know how we can help you better! You can send us a question: 
				<a href="mailto:info@pcex.io" target="_top" style="color: inherit;text-decoration: none;">info@pcex.io</a>
			</p>
		</div>
    </div>`;
    
 
    this.sendHtml(email,"PCEX Membership Disapproval",msg);
}

  exports.Emailer= Emailer;